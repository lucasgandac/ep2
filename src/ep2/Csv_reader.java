
package ep2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Csv_reader {
	static public ArrayList<Pokemon> listadepokemon = new ArrayList<Pokemon>() ;
		{
		String csvFile1 = "POKEMONS_DATA_1.csv";
        String csvFile2 = "POKEMONS_DATA_2.csv";
        String linha = ""; String linha2 = "";
        String virgula = ",";
        BufferedReader arquivo1 = null;
        BufferedReader arquivo2 = null;

        try {//Abrindo os arquivos csv
         arquivo1 = new BufferedReader(new FileReader(csvFile1)) ;
         arquivo2 = new BufferedReader(new FileReader(csvFile2)) ;
         
         
             linha = arquivo1.readLine();
             linha2 = arquivo2.readLine();
        	 while (((linha = arquivo1.readLine()) != null ) | ( (linha2 = arquivo2.readLine()) != null)) { //Criando uma lista a partir dos arquivos CSV
        		 String[] num = linha.split(virgula);
        		 String[] num2 = linha2.split(virgula);
        		 ArrayList<String> habilidades, movimentos;
        		 habilidades= new ArrayList<String>();
        		 movimentos = new ArrayList<String>();
        		for(int f = 8; f <= 14; f++)
        		{
        		 movimentos.add(num2[f]);
        		}
        		for(int f = 5; f <=7; f++)
        		{
        		 habilidades.add(num2[f]);
        		}
        	 listadepokemon.add(new Pokemon(num[0], num[1],num[2], num[3], num[4], num[5], num[6], num[7],
     				num[8], num[9], num[10], num[11], num[12], num2[3], num2[4], habilidades, movimentos));  
                  	 }
        }
        
        
                catch (IOException e) {
            e.printStackTrace();
        }
        catch(NullPointerException e)
        {
            System.out.print("NullPointerException Caught"); }
        finally {
        		if( arquivo1 != null) {
        			try {
        				arquivo1.close();
        			}
        			 catch (IOException e) {
        	            e.printStackTrace();
        	        }
        			
        			}
        		if( arquivo2 != null) {
        			try {
        				arquivo2.close();
        			}
        			 catch (IOException e) {
        	            e.printStackTrace();
        	        }
        			
        			}
        		
        		}
	}
        
}

